# High-order Unfitted Discretizations for PDEs Coupled with Geometric Flow

Welcome to the repository for reproducibility of the numerical results in the doctoral thesis of Yimin Lou. In my thesis, Chapter 3, Chapter 4, Chapter 5, and Chapter 6 contain some numerical experiments or simulations, for which we wrote python codes to work with NGSolve/ngsxfem. This repository accordingly consists of the corresponding files, in which you can find the python codes for reproducibility of the numerical results. 

### Unfitted Isoparametric BDF-Eulerian Finite Element Method

In Chapter 3, we develop the unfitted isoparametric BDF-Eulerian FEM to solve the advection-diffusion equation on an evolving domain. This file is basically a folk from https://gitlab.gwdg.de/lehrenfeld/repro-isop-unf-bdf-fem.git

### Stabilized Isoparametric Trace Finite Element Method

In Chapter 4, we develop the stabilized isoparametric TraceFEM to solve the mean curvature vector of a hypersurface. This file contains the python codes for convergence study for the discrete mean curvature vector of a circle and a torus. 

### Runge-Kutta Discontinuous Galerkin Finite Element Method

In Chapter 5, we introduce the RKDG FEM to solve the advection equation of a level set function transported by an extended velocity field. This file includes the python code for convergence study of the 4th-order RKDG method. 

### Coupled High-order Discretizations with Coupling Algorithms

In Chapter 6, we propose the weak and the strong coupling algorithms to integrate the aforementioned high-order discretizations towards proof-of-concept numerical simulation of the coupled bulk-surface osmosis free boundary problem. This file includes the python codes of the stabilized isoparametric TraceFEM coupled with the 4th-order RKDG method for a shrinking circle mean curvature flow, and the all-coupled method for the osmosis model.  

## Bottom Line
This repository is built for the purpose of reproducibility only. Although the codes are all tested, some scripts are yet to be polished, and the NGSolve/ngsxfem package may be updated in the future, so we do not guarantee the successful reproduction of all the numerical results at all times. 

