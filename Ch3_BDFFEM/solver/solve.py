from netgen.geom2d import SplineGeometry
from ngsolve import *
from xfem import *
from xfem.lsetcurv import *
from math import pi, ceil
import numpy as np

ngsglobals.msg_level = 0

error_types = ["l2L2","l2H1","linfL2"]


def CheckElementHistory(els_hasneg_current, els_extension_old):
    """
    Checks if all current elements have the necessary history for BDF-r

    Parameters
    ----------
    els_hasneg_current : ngsolve.ngstd.BitArray
            Elements which require a history
    els_extention_old  : list of ngsolve.ngstd.BitArray
            Extension elements at previous steps

    Returns
    -------
    None

    Raises
    ------
    Exception
            If some elements do not have the required history for the BDF scheme used.
    """
    els_test = BitArray(len(els_hasneg_current))
    for els_ext_old in els_extension_old:
        els_test[:] = els_hasneg_current & (~els_ext_old)
        if sum(els_test) > 0:
            raise Exception("Some elements have no history !!!")
    return None

def SolveOneDomainProblem(options, Lxs=None, Lts=None, LxLts=None, quiet=False, draw=True):
    if quiet:
        def myprint(*args,**kwargs):
            pass
    else:
        myprint = print
    
    do_vtk = options["vtkout"]
    example = options["example"]
    
    import importlib
    example = importlib.import_module(example)
    k = options["k"]
    # BDF-r method with r:
    r = options["r"]
    
    q = options["q"] if "q" in options else -1
    if q == -1:
        q = k
        
    bdf_coef_table=[[1,-1],
                    [3/2,-2,1/2],
                    [11/6,-3,3/2,-1/3],
                    [25/12,-4,3,-4/3,1/4],
                    [137/60,-5,5,-10/3,5/4,-1/5],
                    ]
    bdf_coefs=bdf_coef_table[r-1]
    
    # Time parameters
    t = Parameter(0.0)
    
    # Problem and discretisation parameters
    #nu = 1e-5
    q = options["q"] if "q" in options else -1
    c_gamma = options["c_gamma"] if "c_gamma" in options else 1
    
    # Sparse direct solver
    # inverse = "pardiso"
    
    background_domain = SplineGeometry()
    background_domain.AddRectangle(example.lowerleft, example.upperright)
    
    u_ex = example.solution_function(t)
    grad_u_ex = example.grad_solution_function(t)
    rhs = example.rhs_function(t)
    w = example.velocity_function(t)
    div_w = example.div_w_function(t)
    
    if LxLts:
        Lxs = [q[0] for q in LxLts]
    
    tm = TaskManager()
    tm.__enter__()
    ngmesh = background_domain.GenerateMesh(maxh=example.maxh, quad_dominated=False)
    lastLx = 0

    current_error = dict([(et, float("nan")) for et in error_types])
    errors = dict([(et, np.empty((len(Lxs),len(Lts)),dtype=np.float)) for et in error_types]) 

    for iLx, Lx in enumerate(Lxs):
        for i in range(lastLx,Lx):
            ngmesh.Refine()
        lastLx = Lx
        mesh = Mesh(ngmesh)
        h_max = example.maxh * 0.5**Lx
        
        # FE Space
        V = H1(mesh, order=k, dgjumps=True)
        gfu_old = [GridFunction(V) for i in range(r)]
        gfu = gfu_old[0]
        
        # Higher order discrete level set approximation
        lsetmeshadap = LevelSetMeshAdaptation(mesh, order=k, threshold=0.1,
                                              discontinuous_qn=True)
        lsetmeshadap.gf_to_project = []
        deformation = lsetmeshadap.deform
        lsetp1 = lsetmeshadap.lset_p1
        lsetp1old = GridFunction(lsetmeshadap.lset_p1.space)

        if draw:
            scene = DrawDC(lsetp1,gfu,-5,mesh,"gfu", deformation = deformation, autoscale=False, min=example.visualization_min, max=example.visualization_max)
        
        # Cut-Info classes for element marking
        ci_main = CutInfo(mesh)
        ci_inner = CutInfo(mesh)
        ci_outer = CutInfo(mesh)
        
        # Element and facet markers
        els_hasneg = ci_main.GetElementsOfType(HASNEG)
        els_outer = ci_outer.GetElementsOfType(HASNEG)
        els_inner = ci_inner.GetElementsOfType(NEG)
        
        els_ring = BitArray(mesh.ne)
        facets_ring = BitArray(mesh.nedge)
        els_outer_old = [BitArray(mesh.ne) for l in range(r)]
            
        vtk = VTKOutput(ma=mesh,coefs=[lsetp1,CoefficientFunction((deformation[0],deformation[1],0)),gfu,BitArrayCF(els_outer)],names=["lsetp1","deformation","gfu","active"],filename="vtkout_",subdivision=3)
        
        if LxLts:
            Lts = LxLts[iLx][1:]

        for iLt, Lt in enumerate(Lts):
            dt = example.dt_0 * 0.5**Lt
            
            # Discretisation
            delta = r * dt * example.velmax()
            myprint("delta: ", delta)
            K = int(ceil(delta / h_max))
            gamma_s = c_gamma * K
            
            u, v = V.TnT()
            h = specialcf.mesh_size
            
            dx = dCut(levelset=lsetp1, domain_type=NEG, definedonelements=els_hasneg,
                      deformation=deformation)
            dw = dFacetPatch(definedonelements=facets_ring, deformation=deformation)
            
            # Bilinear and linear forms
            a = RestrictedBilinearForm(V, element_restriction=els_outer,
                                       facet_restriction=facets_ring, check_unused=False)
        
            a += (bdf_coefs[0] / dt) * u * v * dx
            a += example.nu * InnerProduct(grad(u), grad(v)) * dx
            a += (InnerProduct(w, grad(u)) * v + div_w * u * v) * dx
            a += gamma_s * (1 / h**2) * (u - u.Other()) * (v - v.Other()) * dw
            f = LinearForm(V)
            f += rhs * v * dx
            f += sum([(-bdf_coefs[i+1] / dt) * gfu_old[i] for i in range(r)]) * v * dx
            
            # Error computation
            errors_L2, errors_H1= [], []
            dx_2k = dx.order(2 * k)

            if do_vtk:
                vtk.Do()
            
            def CompErrs():
                l2 = sqrt(Integrate((gfu - u_ex)**2 * dx_2k, mesh))
                h1 = sqrt(Integrate((grad(gfu) - grad_u_ex)**2 * dx_2k, mesh))
            
                errors_L2.append(l2)
                errors_H1.append(h1)
            
                return l2
            
            
            # Project the solution defined with respect to the last mesh deformation
            # onto the the mesh with the current mesh deformation.
            lsetmeshadap.ProjectOnUpdate(gfu_old)
            
            # Initialize gfu_old
            for i in range(r):
                t.Set(-i * dt)
                gfu_old[i].Set(u_ex)
            t.Set(0)
            
            InterpolateToP1(example.levelset_function(0) - delta, lsetp1)
            ci_outer.Update(lsetp1)
            InterpolateToP1(example.levelset_function(0) + delta, lsetp1)
            ci_inner.Update(lsetp1)
            InterpolateToP1(example.levelset_function(0), lsetp1)
            ci_main.Update(lsetp1)

            for l in range(r):
                els_outer_old[l][:] = True
            els_outer[:] = True
            
            deformation = lsetmeshadap.CalcDeformation(example.levelset_function(t))
            if do_vtk:
                vtk.Do()
            Nend = 2**Lt + 1
            for it in range(1, Nend):
                for l in range(r-1):
                    els_outer_old[r-1-l][:] = els_outer_old[r-2-l]
                els_outer_old[0][:] = els_outer
                t.Set(it * dt)
                # Update mesh deformation (Note: this updates gfu_old (including gfu))
                deformation = lsetmeshadap.CalcDeformation(example.levelset_function(t))
            
                # Update Element markers: extension facets. Updating the cut-info
                # classes automatically updates the marker BitArrays.
                InterpolateToP1(example.levelset_function(t) - delta, lsetp1)
                ci_outer.Update(lsetp1)
                InterpolateToP1(example.levelset_function(t) + delta, lsetp1)
                ci_inner.Update(lsetp1)
            
                els_ring.Clear(), facets_ring.Clear()
                els_ring |= els_outer & ~els_inner
                facets_ring |= GetFacetsWithNeighborTypes(mesh, a=els_outer, b=els_ring, bnd_val_a=False, bnd_val_b=False)
            
                # Update Element markers: Domain elements
                InterpolateToP1(example.levelset_function(t), lsetp1)
                ci_main.Update(lsetp1)
            
                active_dofs = GetDofsOfElements(V, els_outer)
            
                # Check element history for method of lines time-derivative approx.
                CheckElementHistory(els_hasneg, els_outer_old)
                
                # Update Linear System
                a.Assemble(reallocate=True)
                f.Assemble()
                inv = a.mat.Inverse(active_dofs)
                # Solve Problem
                for i in range(r-1):
                    gfu_old[r-1-i].vec.data = gfu_old[r-i-2].vec
                
                gfu.vec.data = inv * f.vec
        
                # Compute Errors
                l2err= CompErrs()
            
                myprint(f"\rLx={Lx}, dt={dt:4.2f}, t={t.Get():4.2f}, ", end="")
                myprint(f"E(L2) = {l2err:4.2e}, ", end="")
                myprint(f"act_els = {sum(els_outer)}, K = {K}", end="")
                if it in [i * 2**(Lt-3) for i in range(1,9)]:
                    if draw:
                        scene.Redraw()
                        Redraw()
                    if do_vtk:
                        vtk.Do()
                        myprint(f"\n VTK output at : {t.Get()}")
                        
            # Post Processing
            current_error["l2L2"] = sqrt(dt * sum([e**2 for e in errors_L2]))
            current_error["l2H1"] = sqrt(dt * sum([e**2 for e in errors_H1]))
            current_error["linfL2"] = max(errors_L2)

            for et in error_types:
                errors[et][iLx][iLt] = current_error[et]
            
            myprint("\rLx=",Lx,", Lt=", Lt, end=", ")#,":  L2(L2) Error = {:6.4e}".format(err_l2l2),end=" ")
            for et in error_types:
                myprint("E(" + et + ") = {:4.2e}".format(current_error[et]),end=" ")
            myprint("")
        myprint("")
        
    tm.__exit__(None,None,None)
    return errors

def SolveInterfaceProblem(options, Lxs=None, Lts=None, LxLts=None, quiet=False, draw=True):
    if quiet:
        def myprint(*args,**kwargs):
            pass
    else:
        myprint = print
    
    do_vtk = options["vtkout"]
    example = options["example"]
    
    import importlib
    example = importlib.import_module(example)
    k = options["k"]
    # BDF-r method with r:
    r = options["r"]
    
    q = options["q"] if "q" in options else -1
    if q == -1:
        q = k
        
    bdf_coef_table=[[1,-1],
                    [3/2,-2,1/2],
                    [11/6,-3,3/2,-1/3],
                    [25/12,-4,3,-4/3,1/4],
                    [137/60,-5,5,-10/3,5/4,-1/5],
                    ]
    bdf_coefs=bdf_coef_table[r-1]
    
    # Time parameters
    t = Parameter(0.0)
    
    # Problem and discretisation parameters
    #nu = 1e-5
    c_gamma = options["c_gamma"] if "c_gamma" in options else 1
    lambda_N = options["lambda_N"] if "lambda_N" in options else 40
    
    # Sparse direct solver
    # inverse = "pardiso"
    
    background_domain = SplineGeometry()
    background_domain.AddRectangle(example.lowerleft, example.upperright)
    
    u_ex = example.solution_function(t)
    grad_u_ex = example.grad_solution_function(t)
    rhs = example.rhs_function(t)
    w = example.velocity_function(t)
    div_w = example.div_w_function(t)
    
    if LxLts:
        Lxs = [q[0] for q in LxLts]
    
    tm = TaskManager()
    tm.__enter__()
    ngmesh = background_domain.GenerateMesh(maxh=example.maxh, quad_dominated=False)
    lastLx = 0

    current_error = dict([(et, float("nan")) for et in error_types])
    errors = dict([(et, np.empty((len(Lxs),len(Lts)),dtype=np.float)) for et in error_types]) 

    for iLx, Lx in enumerate(Lxs):
        for i in range(lastLx,Lx):
            ngmesh.Refine()
        lastLx = Lx
        mesh = Mesh(ngmesh)
        h_max = example.maxh * 0.5**Lx
        
        # FE Space
        W = H1(mesh, order=k, dgjumps=True, dirichlet=".*")
        V = W*W
        gfu_old = [GridFunction(V) for i in range(r)]
        gfu = gfu_old[0]
        
        # Higher order discrete level set approximation
        lsetmeshadap = LevelSetMeshAdaptation(mesh, order=k, threshold=0.1,
                                              discontinuous_qn=True)
        lsetmeshadap.gf_to_project = []
        deformation = lsetmeshadap.deform
        lsetp1 = lsetmeshadap.lset_p1
        lsetp1old = GridFunction(lsetmeshadap.lset_p1.space)

        if draw:
            scene = DrawDC(lsetp1,gfu.components[0],gfu.components[1],mesh,"gfu", deformation = deformation, autoscale=False, min=example.visualization_min, max=example.visualization_max)
        
        # Cut-Info classes for element marking
        ci_main = CutInfo(mesh)
        ci_inner = CutInfo(mesh)
        ci_outer = CutInfo(mesh)
        
        # Element and facet markers
        els_if = ci_main.GetElementsOfType(IF)
        els_has = [ci_main.GetElementsOfType(dt) for dt in [HASNEG, HASPOS]]
        els_outer = [ci_outer.GetElementsOfType(HASNEG), ci_inner.GetElementsOfType(HASPOS)]
        els_inner = [ci_inner.GetElementsOfType(   NEG), ci_outer.GetElementsOfType(   POS)]
        
        els_ring = [BitArray(mesh.ne),BitArray(mesh.ne)]
        facets_ring = [BitArray(mesh.nedge),BitArray(mesh.nedge)]
        facets_rings = BitArray(mesh.nedge)
        els_outer_old = [[BitArray(mesh.ne) for l in range(r)],[BitArray(mesh.ne) for l in range(r)]]

        vtk = VTKOutput(ma=mesh,coefs=[lsetp1,CoefficientFunction((deformation[0],deformation[1],0)),gfu.components[0],gfu.components[0],BitArrayCF(els_outer[0]),BitArrayCF(els_outer[1])],names=["lsetp1","deformation","gfu_inner","gfu_outer","active_inner","active_outer"],filename="vtkout_",subdivision=3)
        
        if LxLts:
            Lts = LxLts[iLx][1:]

        for iLt, Lt in enumerate(Lts):
            tau = example.dt_0 * 0.5**Lt
            
            # Discretisation
            delta = r * tau * example.velmax()
            myprint("delta: ", delta)
            K = int(ceil(delta / h_max))
            gamma_s = c_gamma * K
            
            u, v = V.TnT()
            h = specialcf.mesh_size
            
            dx = [dCut(levelset=lsetp1, domain_type=dt, definedonelements=els_has[dt],
                      deformation=deformation) for dt in [NEG,POS]]
            ds = dCut(levelset=lsetp1, domain_type=IF, definedonelements=els_if,
                      deformation=deformation)
            dw = [dFacetPatch(definedonelements=facets_ring_i, deformation=deformation) for facets_ring_i in facets_ring]

            alpha = example.alpha
            beta = example.beta

            def bjump(w):
                return beta[0]*w[0]-beta[1]*w[1]
            def avg_flux(w):
                return 0.5*(-alpha[0]*grad(w[0])*n-alpha[1]*grad(w[1])*n)
            # Bilinear and linear forms
            n = Normalize(grad(lsetp1))
            a = BilinearForm(V, check_unused=False)
            a = RestrictedBilinearForm(V, facet_restriction=facets_rings, check_unused=False)
            a += sum([(bdf_coefs[0] / tau) * beta[i] * u[i] * v[i] * dx[i] for i in [0,1]])
            a += sum([alpha[i] * beta[i] * InnerProduct(grad(u[i]), grad(v[i])) * dx[i] for i in [0,1]])
            a += sum([beta[i] * (InnerProduct(w, grad(u[i])) + div_w * u[i] ) * v[i] * dx[i] for i in [0,1]])
            a += sum([gamma_s * alpha[i] * beta[i] * (1 / h**2) * (u[i] - u[i].Other()) * (v[i] - v[i].Other()) * dw[i] for i in [0,1]])
            a += (avg_flux(u) * bjump(v) + avg_flux(v) * bjump(u) + lambda_N / h * 0.5* ( alpha[0] + alpha[1] ) * bjump(u) * bjump(v)) * ds
                     
            f = LinearForm(V)
            f += sum([rhs[i] * beta[i] * v[i] * dx[i] for i in [0,1]])
            f += sum([sum([(-bdf_coefs[j+1] / tau) * gfu_old[j].components[i] for j in range(r)]) * beta[i] * v[i] * dx[i] for i in [0,1]])
            
            # Error computation
            errors_L2, errors_H1 = [], []
            dx_2k = [dx[i].order(2 * k) for i in [0,1]]
            if do_vtk:
                vtk.Do()
            
            def CompErrs():
                l2 = sqrt(sum([Integrate((gfu.components[i] - u_ex[i])**2 * dx_2k[i], mesh) for i in [0,1]]))
                h1 = sqrt(sum([Integrate((grad(gfu.components[i]) - grad_u_ex[i])**2 * dx_2k[i], mesh) for i in [0,1]]))
            
                errors_L2.append(l2)
                errors_H1.append(h1)
            
                return l2
            
            
            # Project the solution defined with respect to the last mesh deformation
            # onto the the mesh with the current mesh deformation.
            for gf_old in gfu_old:
                lsetmeshadap.ProjectOnUpdate(list(gf_old.components))
            
            # Initialize gfu_old
            for l in range(r):
                t.Set(-l * tau)
                for i in [0,1]:
                    gfu_old[l].components[i].Set(u_ex[i])
            t.Set(0)
            
              
            InterpolateToP1(example.levelset_function(0) - delta, lsetp1)
            ci_outer.Update(lsetp1)
            InterpolateToP1(example.levelset_function(0) + delta, lsetp1)
            ci_inner.Update(lsetp1)
            InterpolateToP1(example.levelset_function(0), lsetp1)
            ci_main.Update(lsetp1)

            for i in [0,1]:
                for l in range(r):
                    els_outer_old[i][l][:] = True
                els_outer[i][:] = True
            
            deformation = lsetmeshadap.CalcDeformation(example.levelset_function(t))
            if do_vtk:
                vtk.Do()
            Nend = 2**Lt + 1
            for it in range(1, Nend):
                
                for i,dt  in enumerate([NEG,POS]):
                    for l in range(r-1):
                        els_outer_old[i][r-1-l][:] = els_outer_old[i][r-2-l]
                    els_outer_old[i][0][:] = els_outer[i]
                t.Set(it * tau)
                # Update mesh deformation (Note: this updates gfu_old (including gfu))
                deformation = lsetmeshadap.CalcDeformation(example.levelset_function(t))
            
                # Update Element markers: extension facets. Updating the cut-info
                # classes automatically updates the marker BitArrays.
                InterpolateToP1(example.levelset_function(t) - delta, lsetp1)
                ci_outer.Update(lsetp1)
                InterpolateToP1(example.levelset_function(t) + delta, lsetp1)
                ci_inner.Update(lsetp1)

                for i in [0,1]:
                    els_ring[i].Clear(), facets_ring[i].Clear()
                    els_ring[i] |= els_outer[i] & ~els_inner[i]
                    facets_ring[i] |= GetFacetsWithNeighborTypes(mesh, a=els_outer[i], b=els_ring[i], bnd_val_a=False, bnd_val_b=False)
                facets_rings[:] = facets_ring[0] | facets_ring[1]
                # Update Element markers: Domain elements
                InterpolateToP1(example.levelset_function(t), lsetp1)
                ci_main.Update(lsetp1)
            
                active_dofs = CompoundBitArray([GetDofsOfElements(W, els_outer[i]) for i in [0,1]])
            
                # Check element history for method of lines time-derivative approx.
                for i in [0,1]:
                    CheckElementHistory(els_has[i], els_outer_old[i])
            
                # Update Linear System
                a.Assemble(reallocate=True)
                f.Assemble()
                inv = a.mat.Inverse(active_dofs & V.FreeDofs())
                # Solve Problem
                for i in range(r-1):
                    gfu_old[r-1-i].vec.data = gfu_old[r-i-2].vec
                for i in [0,1]:
                    gfu.components[i].Set(u_ex[i],BND)
                f.vec.data -= a.mat * gfu.vec
                gfu.vec.data += inv * f.vec
        
                # Compute Errors
                l2err = CompErrs()
            
                myprint(f"\rLx={Lx}, dt={tau:4.2f}, t={t.Get():4.2f}, ", end="")
                myprint(f"E(L2) = {l2err:4.2e}, ", end="")
                myprint(f"act_els = {sum(els_outer[0])+sum(els_outer[1])}, K = {K}", end="")

                if it in [i * 2**(Lt-3) for i in range(1,9)]:
                    if draw:
                        scene.Redraw()
                        Redraw()
                    if do_vtk:
                        vtk.Do()
                        myprint(f"\n VTK output at : {t.Get()}")
                        
            # Post Processing
            current_error["l2L2"] = sqrt(tau * sum([e**2 for e in errors_L2]))
            current_error["l2H1"] = sqrt(tau * sum([e**2 for e in errors_H1]))
            current_error["linfL2"] = max(errors_L2)

            for et in error_types:
                errors[et][iLx][iLt] = current_error[et]
            
            myprint("\rLx=",Lx,", Lt=", Lt, end=", ")#,":  L2(L2) Error = {:6.4e}".format(err_l2l2),end=" ")
            for et in error_types:
                myprint("E(" + et + ") = {:4.2e}".format(current_error[et]),end=" ")
            myprint("")
        myprint("")
        
    tm.__exit__(None,None,None)
    return errors


def Solve(options, Lxs=None, Lts=None, LxLts=None, quiet=False):
    if options["example"] == "intfprob":
        errors = SolveInterfaceProblem(options,Lxs=Lxs,Lts=Lts)
    else:
        errors = SolveOneDomainProblem(options,Lxs=Lxs,Lts=Lts)
    return errors
