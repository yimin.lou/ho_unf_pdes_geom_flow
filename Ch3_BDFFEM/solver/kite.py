# Deforming bubble

from ngsolve import *
from math import pi, ceil

name = "kite"

maxh = 0.25

nu = 1
T = 1
dt_0 = T

lowerleft = [-1,-1]
upperright = [1,1]

R0 = 0.5
C = 5/3
W = 1/6

visualization_min = 0
visualization_max = 1

def rho_function(t):
    return (W-C*y*y)*t
#    return (1-2*pi*sin(pi*t)*y*y)*(1-cos(pi*t))
def rho_x(t): 
    return CoefficientFunction(0)
def rho_y(t):
    return -2*C*y*t
#    return 4*pi*y*sin(pi*t)*(cos(pi*t)-1)
def rho_xx(t): 
    return CoefficientFunction(0)
def rho_yy(t):
    return -2*C*t
#    return 4*pi*sin(pi*t)*(cos(pi*t)-1)
def rho_t(t):
    return W-C*y*y
#    return pi*(sin(pi*t)*(1-2*pi*y*y*sin(pi*t))+2*pi*y*y*(cos(pi*t)-1)*cos(pi*t))
    
def velocity_function(t):
    return CoefficientFunction((rho_t(t),0))
def velmax():
    return C
def div_w_function(t):
    return CoefficientFunction(0)
    
def r_function(x,y,t):
    return sqrt((x-rho_function(t))*(x-rho_function(t))+y*y)
def r_x(x,y,t):
    return (x-rho_function(t))/r_function(x,y,t)*(1-rho_x(t))
#def r_xx(x,y,t):
#    return ((1-rho_x(t))**2-(x-rho_function(t))*rho_xx(t))/r_function(x,y,t) - ((x-rho_function(t))*(1-rho_x(t)))**2/r_function(x,y,t)**3
def r_y(x,y,t):
    return (y-(x-rho_function(t))*rho_y(t))/r_function(x,y,t)
#def r_yy(x,y,t):
#    return (1+rho_y(t)**2-(x-rho_function(t))*rho_yy(t))/r_function(x,y,t) - ((x-rho_function(t))*rho_y(t)-y)**2/r_function(x,y,t)**3
def r_t(x,y,t):
    return -rho_t(t)*(x-rho_function(t))/r_function(x,y,t)

def R(t):
    return R0*exp(0)
def levelset_function(t):
    return r_function(x,y,t) - R(t)
def s(x,y,t):
    return sin(pi*r_function(x,y,t)/R(t))
def c(x,y,t):
    return cos(pi*r_function(x,y,t)/R(t))
def solution_function(t):
    return c(x,y,t)
def grad_solution_function(t):
    return -s(x,y,t)*pi/R(t) * CoefficientFunction((r_x(x,y,t),r_y(x,y,t)))
#def deriv_solution_function(t):
#    return (r_function(x,y,t)-r_t(x,y,t))*pi*s(x,y,t)/R(t)
#def laplace_solution_function(t):
#    return pi*(R(t)*(r_xx(x,y,t)+r_yy(x,y,t))*s(x,y,t)+pi*(r_x(x,y,t)**2+r_y(x,y,t)**2)*c(x,y,t))/R(t)**2

def rhs_function(t): 
    return - nu*(solution_function(t).Diff(x).Diff(x) + solution_function(t).Diff(y).Diff(y))

