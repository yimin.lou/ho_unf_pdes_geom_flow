from math import pi
from ngsolve import *
from ngsolve.internal import *
from xfem import *
from xfem.lsetcurv import *
from netgen.geom2d import *
# import matplotlib.pyplot as plt

domain = SplineGeometry()
domain.AddRectangle([-1.2,-1.2],[1.2,1.2], bc = 1)
mesh = Mesh(domain.GenerateMesh(maxh = 0.6, quad_dominated = False))
Draw(mesh)

l = []
q = []
j = 4
dt = 1.e-2

for i in range(j): 
    if i > 0:
        mesh.Refine()
        dt /= 4
    # Draw(phi, mesh, "levelset")
        
    c = 0.25
    d = 0.4
    r = sqrt(x*x + y*y)
    phi = lambda x,y: ((x-d)*(x-d) + (y-d)*(y-d)) - c*c
    # psi = lambda x,y: sqrt(phi(x,y)*phi(x,y)) - 0.05
    
    Draw(phi(x,y), mesh, "phi")
    # Draw(psi(x,y), mesh, "psi")
    
    tt = 1.0
    t = Parameter(0)
    o = IfPos(0.5*tt-t,1,-1)
    k = IfPos(r-1,0,1-r*r)
    w = o*k*CoefficientFunction((-y,x))
    n = specialcf.normal(mesh.dim)
    h = specialcf.mesh_size
    # Draw(w, mesh, "velocity")
    
    # ubnd = IfPos(x,1,0)
    # ubnd = CoefficientFunction(0)
    ub = phi
    
    p = 1
    
    Vh = L2(mesh, order = p, dirichlet = [], dgjumps=True)
    
    u = Vh.TrialFunction()
    v = Vh.TestFunction()

    a = BilinearForm(Vh, symmetric = False)
    a += SymbolicBFI(form = w * grad(u) * v, element_boundary = False)
    a += SymbolicBFI(form = w * n * (IfPos(w*n,u,u.Other(ub)) - u) * v, element_boundary = True)
    a.Assemble()

    num = GridFunction(Vh)
    num.Set(phi(x,y))
    Draw(num, mesh, "numeric")
    
    x0 = lambda t: x*cos(k*t) + y*sin(k*t)
    y0 = lambda t: y*cos(k*t) - x*sin(k*t)
    exa = lambda t: phi(x0(t),y0(t))
    exa = IfPos(0.5*tt-t,exa(t),exa(tt-t))
    Draw(exa, mesh, "exact")
    
    #cov = lambda t: psi(x0(t),y0(t))
    #cov = IfPos(0.5*tt-t,cov(t),cov(tt-t))
    #Draw(cov, mesh, "cover")
    
    gamad = LevelSetMeshAdaptation(mesh, order = p)#, threshold=1e-2)
    deformation = gamad.CalcDeformation(num)
    gamip = gamad.lset_p1
    subdiv = 0
    gamma = {"levelset": gamip, "domain_type": IF, "subdivlvl": subdiv}
    
    # covip = GridFunction(H1(mesh,order=1))
    # InterpolateToP1(exa, covip)
    
    visoptions.deformation = 0
    visoptions.autoscale = 0
    visoptions.mminval = 0
    visoptions.mmaxval = 0
    
    f0 = num.vec.CreateVector()
    f1 = num.vec.CreateVector()
    f2 = num.vec.CreateVector()
    f3 = num.vec.CreateVector()
    utn = num.vec.CreateVector()
    res = num.vec.CreateVector()
    a.Apply(num.vec, res)
    Vh.SolveM(rho=CoefficientFunction(1), vec=res)
    
    # Explicit

    time = 0.0
    tstep = 0
    with TaskManager(): 

        while time < tt: 
        
            #print("time = ", time)
            t.Set(time)
            utn.data = num.vec
            a.Apply(utn, res) # res = A*num
            Vh.SolveM(rho=CoefficientFunction(1), vec=res) # res = Inv(M)*A*num
            # num.vec.data -= dt * res
            f0.data = -res
            # num.vec.data -= f0
            
            t.Set(time + 0.5*dt)
            utn.data = num.vec + 0.5*dt*f0
            a.Apply(utn, res)
            Vh.SolveM(rho=CoefficientFunction(1), vec=res)
            f1.data = -res
            
            t.Set(time + 0.5*dt)
            utn.data = num.vec + 0.5*dt*f1
            a.Apply(utn, res)
            Vh.SolveM(rho=CoefficientFunction(1), vec=res)
            f2.data = -res
            
            t.Set(time + dt)
            utn.data = num.vec + dt*f2
            a.Apply(utn, res)
            Vh.SolveM(rho=CoefficientFunction(1), vec=res)
            f3.data = -res
            
            num.vec.data += dt/6 * (f0 + 2*f1 + 2*f2 + f3)
            
            time += dt
            tstep += 1
            
            deformation = gamad.CalcDeformation(num)
            gamip = gamad.lset_p1
            gamma = {"levelset": gamip, "domain_type": IF, "subdivlvl": subdiv}
            
            mesh.SetDeformation(deformation)
            error = sqrt(Integrate(gamma, exa*exa, mesh))
            mesh.UnsetDeformation()
            
            #print("L2 Error: {0}".format(error))

            l.append((time, error))
            Redraw(blocking = True)
            
        if i > 0: print("EOC: {0}".format(log(error/errold)/log(1/2)))
        errold = error
            
    mesh.SetDeformation(deformation)
    phase = sqrt(Integrate(gamma, exa*exa, mesh))
    mesh.UnsetDeformation()
    q.append((i, phase))


"""

plt.yscale('log')
plt.xlabel("time (level)")
plt.ylabel("error")
tim, err = zip(*l)
plt.plot(tim, err, ".")
lvl, ero = zip(*q)
plt.plot(lvl, ero, "-*")
# j = 5 # number of reference plots
ref = [[q[0][1]*0.5**(J*Q) for Q in range(len(q))] for J in range(j+3)]
for i in range(j+3): 
    plt.plot(lvl, ref[i])
plt.title("Error vs Time ("+str(j)+" levels, "+str(p)+" order)")
plt.ion()
plt.show()
plt.savefig("error with order =" + str(p) + ".png")


"""

# Implicit

"""
m = BilinearForm(Vh, symmetric = False, flags = {})
m += SymbolicBFI(form = u * v, element_boundary = False)
m.Assemble()

mta = m.mat.CreateMatrix()
mta.AsVector().data = m.mat.AsVector() + dt * a.mat.AsVector()
invmta = mta.Inverse(freedofs = Vh.FreeDofs())

time = 0.0
res = sol.vec.CreateVector()

with TaskManager(): 
    
    while time < tt: 
        
        t.Set(time)
        a.Assemble()
        
        res.data = -dt * a.mat * sol.vec
        sol.vec.data += invmta * res
        
        time += dt
    
        Redraw(blocking = True)
"""
"""
ref = [[l[0][1]*0.5**(j*L) for L in range(len(l))] for j in range(ij)]

plt.yscale('log')
plt.xlabel("lvl")
plt.ylabel("L2 error")
lvl, err = zip(*l)
plt.plot(lvl, err, "-*")

for j in range(ij):
    plt.plot(lvl, ref[j])
plt.title("order = " + str(p))
plt.ion()
plt.show()
plt.savefig("levelset motion with order = " + str(p) + ".png")
input("<press enter to quit>")
"""

