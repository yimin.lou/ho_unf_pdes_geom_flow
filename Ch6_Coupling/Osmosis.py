# ------------------------------ LOAD LIBRARIES -------------------------------
from math import pi
from ngsolve import *
from netgen.geom2d import *
from xfem import *
from xfem.lsetcurv import *
import numpy as np
from numpy import savetxt, concatenate, array, c_

# -------------------------------- PARAMETERS ---------------------------------
# Time
t = Parameter(0)
# Final time
T = pi
# Time step
dt = T/50
# Mesh diameter
hmax = 0.2
#
# Polynomial order of finite element discretization
k = 1
# Polynomial order of discontinuous Galerkin discretization
p = 1
# Polynomial order of isoparametric mapping
q = 1

# Background domain
domain = SplineGeometry()
domain.AddRectangle([-1.2,-1.2],[1.2,1.2])
mesh = Mesh(domain.GenerateMesh(maxh=hmax, quad_dominated=False))
Draw(mesh)

# Radius
r = 1.0
# Center
#c = 0.0
#d = 0.0
# Level set function
levelset = (x-0)*(x-0) + (y-0)*(y-0) - r*r
Draw(levelset, mesh, "levelset", autoscale=False, min=0, max=0, deformation=True)

# ------------------------------ MEAN CURVATURE -------------------------------

# P1 interpolation of the level set function
lset = GridFunction(H1(mesh, order=1))
InterpolateToP1(levelset, lset)
Draw(lset, mesh, "lset_P1", autoscale=False, min=0, max=0, deformation=True)

zero = {"levelset":lset, "domain_type":IF, "subdivlvl":0}
neg = {"levelset":lset, "domain_type":NEG, "subdivlvl":0}
pos = {"levelset":lset, "domain_type":POS, "subdivlvl":0}

# Cut information
ci = CutInfo(mesh, lset)
    
# Class to compute mesh deformation adapted to the level set function
adap = LevelSetMeshAdaptation(mesh, order = q, threshold = 1e1)
deform = adap.CalcDeformation(levelset)
undeform = adap.CalcDeformation(0)
#lset = adap.lset_p1

# Background/trace finite element space and trial/test functions
Vh = H1(mesh, order=k, dirichlet=[], dgjumps=True)
#Vtr = Compress(Vh, GetDofsOfElements(Vh,ci.GetElementsOfType(IF)))
#dof = Vtr.FreeDofs()
cutdof = Vh.FreeDofs() & GetDofsOfElements(Vh,ci.GetElementsOfType(IF))
extdof = ~cutdof
u, v = Vh.TnT()
        
# Unit normal vector and mesh size parameter
n = Normalize(grad(lset))
h = specialcf.mesh_size
        
# Tangential projection and Cartesian unit vectors
P = CoefficientFunction((1-n[0]*n[0], -n[0]*n[1], -n[1]*n[0], 1-n[1]*n[1]), dims=(2,2))
ex = CoefficientFunction((1,0))
ey = CoefficientFunction((0,1))
#P = lambda u: u - (u*n)*n
        
# Measure on cut elements, interface, and facet patch
dx = dx(definedonelements=ci.GetElementsOfType(IF), deformation=deform)
ds = dCut(levelset=lset, domain_type=IF, definedonelements=ci.GetElementsOfType(IF), deformation=deform)
dw = dFacetPatch(definedonelements=GetFacetsWithNeighborTypes(mesh, a=ci.GetElementsOfType(IF),b=ci.GetElementsOfType(IF)), deformation=deform)

# Stabilization coefficient
gamma = 1
    
# Bilinear form
a = BilinearForm(Vh, check_unused=False)
a += SymbolicBFI(levelset_domain = zero, form = u * v, definedonelements = ci.GetElementsOfType(IF))
#a += u * v * ds
# Normal diffusion stabilization
a += SymbolicBFI(form = gamma * h * (grad(u)*n) * (grad(v)*n), definedonelements = ci.GetElementsOfType(IF))
#a += gamma * h * (grad(u)*n) * (grad(v)*n) * dx
# Ghost penalty stabilization
a += SymbolicFacetPatchBFI(form = gamma * h**(-3) * (u - u.Other()) * (v - v.Other()), skeleton = False, definedonelements = GetFacetsWithNeighborTypes(mesh, a = ci.GetElementsOfType(IF), b = ci.GetElementsOfType(IF)))
#a += (gamma * h**(lam-2) * (u - u.Other()) * (v - v.Other())) * dw

# Linear form
fx = LinearForm(Vh)
fx += SymbolicLFI(levelset_domain = zero, form = InnerProduct(P*ex,P*grad(v)), definedonelements = ci.GetElementsOfType(IF))
#fx += InnerProduct(P*ex,P*grad(v)) * ds
fy = LinearForm(Vh)
fy += SymbolicLFI(levelset_domain = zero, form = InnerProduct(P*ey,P*grad(v)), definedonelements = ci.GetElementsOfType(IF))
#fy += InnerProduct(P*ey,P*grad(v)) * ds
    
# Bilinear/linear form assemble
mesh.SetDeformation(deform)
a.Assemble()
fx.Assemble()
fy.Assemble()
mesh.UnsetDeformation()

# Discrete/exact mean curvature vector computation
inv = a.mat.Inverse(cutdof)

Hx = GridFunction(Vh)
Hx.vec[:] = 0.0
Hx.vec.data = inv * fx.vec
    
Hy = GridFunction(Vh)
Hy.vec[:] = 0.0
Hy.vec.data = inv * fy.vec

kappa = sqrt (Hx*Hx + Hy*Hy)
H = CoefficientFunction((Hx,Hy))
Draw(H, mesh, "num", autoscale=True, min=-1, max=1, deformation=deform)

# -------------------------------- EXTENSION ----------------------------------

alpha = 1.0
beta = 1.0

gfu = GridFunction(Vh)
gfu.Set(CoefficientFunction(0))
ux = gfu * InnerProduct(n,ex)
uy = gfu * InnerProduct(n,ey)

w = (-alpha * kappa + beta * gfu) * n

b = BilinearForm(Vh, symmetric=True, check_unused=False)
b += SymbolicBFI(form = (grad(u)*n) * (grad(v)*n), definedonelements = ci.GetElementsOfType(POS)|ci.GetElementsOfType(NEG))

mesh.SetDeformation(deform)
b.Assemble()
mesh.UnsetDeformation()

#rhs = gfu.vec.CreateVector()
#rhs.data = - b.mat * w.vec
#E = gfu.vec.CreateVector()
#E.data = b.mat.Inverse(extdof) * rhs
#w += E

rhs = Hx.vec.CreateVector()
rhs.data = - b.mat * Hx.vec
Ex = Hx.vec.CreateVector()
Ex.data = b.mat.Inverse(extdof) * rhs
Wx = GridFunction(Vh)
Wx.vec[:] = 0.0
Wx.vec.data = Hx.vec + Ex

rhs = Hy.vec.CreateVector()
rhs.data = - b.mat * Hy.vec
Ey = Hy.vec.CreateVector()
Ey.data = b.mat.Inverse(extdof) * rhs
Wy = GridFunction(Vh)
Wy.vec[:] = 0.0
Wy.vec.data = Hy.vec + Ey

Gx = GridFunction(Vh)
Gy = GridFunction(Vh)
G = CoefficientFunction((Gx,Gy))
Gx.vec.data = Wx.vec
Gy.vec.data = Wy.vec
Wx.Set(shifted_eval(Gx,deform,undeform))
Wy.Set(shifted_eval(Gy,deform,undeform))

W = CoefficientFunction((-Wx,-Wy))
Draw(W, mesh, "velocity")
visoptions.deformation = 0
visoptions.autoscale = 0
visoptions.mminval = 0
visoptions.mmaxval = 0

# ---------------------------- LEVELSET TRANSPORT -----------------------------

Vdg = L2(mesh, order = p, dirichlet = [], flags = {"dgjumps": True})
u, v = Vdg.TnT()

phi = GridFunction(Vdg)
phi.Set(levelset)
Draw(phi, mesh, "levelset_HO", autoscale=False, min=0, max=0, deformation=True)

c = BilinearForm(Vh, symmetric = False, flags = {})
c += SymbolicBFI(form = W * grad(u) * v, element_boundary = False)
c += SymbolicBFI(form = W * n * (IfPos(W*n,u,u.Other(phi)) - u) * v, element_boundary = True)
c.Assemble()

R1 = phi.vec.CreateVector()
R2 = phi.vec.CreateVector()
R3 = phi.vec.CreateVector()
R4 = phi.vec.CreateVector()
psi = phi.vec.CreateVector()
res = phi.vec.CreateVector()
c.Apply(phi.vec, res)
Vdg.SolveM(rho=CoefficientFunction(1), vec=res)

time = 0.0

with TaskManager():

    while time < T:
        
        print("time = ", time)
        
        t.Set(time)
        psi.data = phi.vec
        c.Apply(psi, res)
        Vdg.SolveM(rho=CoefficientFunction(1), vec=res)
        R1.data = -res

        t.Set(time + dt/2)
        psi.data = phi.vec + dt/2 * R1
        c.Apply(psi, res)
        Vdg.SolveM(rho=CoefficientFunction(1), vec=res)
        R2.data = -res
            
        t.Set(time + dt/2)
        psi.data = phi.vec + dt/2 * R2
        c.Apply(psi, res)
        Vdg.SolveM(rho=CoefficientFunction(1), vec=res)
        R3.data = -res
            
        t.Set(time + dt)
        psi.data = phi.vec + dt * R3
        c.Apply(psi, res)
        Vdg.SolveM(rho=CoefficientFunction(1), vec=res)
        R4.data = -res
            
        phi.vec.data += dt/6 * (R1 + 2*R2 + 2*R3 + R4)
            
# ------------------------------ MEAN CURVATURE -------------------------------

        deform = adap.CalcDeformation(phi)
        lset = adap.lset_p1
        Draw(lset, mesh, "lset_P1", autoscale=False, min=0, max=0, deformation=True)
        
        ci.Update(lset)
        zero = {"levelset": lset, "domain_type": IF, "subdivlvl": 0}
        
        cutdof.Clear()
        cutdof = Vh.FreeDofs() & GetDofsOfElements(Vh,ci.GetElementsOfType(IF))
        extdof.Clear()
        extdof = ~cutdof
        
        mesh.SetDeformation(deform)
        a.Assemble()
        fx.Assemble()
        fy.Assemble()
        mesh.UnsetDeformation()

        inv = a.mat.Inverse(cutdof)

        Hx = GridFunction(Vh)
        Hx.vec[:] = 0.0
        Hx.vec.data = inv * fx.vec
    
        Hy = GridFunction(Vh)
        Hy.vec[:] = 0.0
        Hy.vec.data = inv * fy.vec

        kappa = sqrt (Hx*Hx + Hy*Hy)
        H = CoefficientFunction((Hx,Hy))
        #Draw(H, mesh, "num", autoscale=True, min=-1, max=1, deformation=deform)

        gfu.Set(CoefficientFunction(0))
        ux = gfu * InnerProduct(n,ex)
        uy = gfu * InnerProduct(n,ey)

        w = (-alpha * kappa + beta * gfu) * n

# -------------------------------- EXTENSION ----------------------------------

        mesh.SetDeformation(deform)
        b.Assemble()
        mesh.UnsetDeformation()

        rhs = Hx.vec.CreateVector()
        rhs.data = - b.mat * Hx.vec
        Ex = Hx.vec.CreateVector()
        Ex.data = b.mat.Inverse(extdof) * rhs
        Wx = GridFunction(Vh)
        Wx.vec[:] = 0.0
        Wx.vec.data = Hx.vec + Ex

        rhs = Hy.vec.CreateVector()
        rhs.data = - b.mat * Hy.vec
        Ey = Hy.vec.CreateVector()
        Ey.data = b.mat.Inverse(extdof) * rhs
        Wy = GridFunction(Vh)
        Wy.vec[:] = 0.0
        Wy.vec.data = Hy.vec + Ey

        Gx = GridFunction(Vh)
        Gy = GridFunction(Vh)
        G = CoefficientFunction((Gx,Gy))
        Gx.vec.data = Wx.vec
        Gy.vec.data = Wy.vec
        Wx.Set(shifted_eval(Gx,deform,undeform))
        Wy.Set(shifted_eval(Gy,deform,undeform))

        W = CoefficientFunction((-Wx,-Wy))
        #Draw(W, mesh, "velocity")
        visoptions.deformation = 0
        visoptions.autoscale = 0
        visoptions.mminval = 0
        visoptions.mmaxval = 0
        
        time += dt
        Redraw(blocking = True)

