from netgen.geom2d import SplineGeometry
from ngsolve import *
from xfem import *
from xfem.lsetcurv import *
from xfem.lset_spacetime import *
from math import pi, factorial
import numpy as np
ngsglobals.msg_level = 0

geom = SplineGeometry()
geom.AddRectangle([-1.2,-1.2],[1.2,1.2])
grid = geom.GenerateMesh(maxh=0.1111, quad_dominated=False)
mesh = Mesh(grid)
Draw(mesh)

n = specialcf.normal(mesh.dim)
h = specialcf.mesh_size
k = 4 # TraceFEM order 
p = 4 # DG-FEM order
q = 5 # Iso-mapping order
gp_type = False # ghost penalty choices: True = derivative version, False = extension version
c = 1
r = sqrt(x*x + y*y)
t = Parameter(0)
#w = -1/sqrt(c*c - 2*t) * CoefficientFunction((x/r,y/r)) # exact velocity field
phi = lambda x,y,t: x*x + y*y - (c*c - 2*t) # exact levelset function

# TraceFEM

lset = GridFunction(H1(mesh,order=1)) # P1 interpolation of levelset
InterpolateToP1(phi(x,y,t),lset)
Draw(lset,mesh,"lsetP1")

itf = {"levelset":lset, "domain_type":IF, "subdivlvl":0}
neg = {"levelset":lset, "domain_type":NEG, "subdivlvl":0}
pos = {"levelset":lset, "domain_type":POS, "subdivlvl":0}

nh = Normalize(grad(lset)) # normal by gradient of levelset (but it is low order???)

citf = CutInfo(mesh,lset)
VT = H1(mesh,order=k,dirichlet=[],dgjumps=True) # Trace space to be cut
#VT = Compress(Vh,GetDofsOfElements(Vh,citf.GetElementsOfType(IF))) 
freedof = VT.FreeDofs()
cutdof = freedof & GetDofsOfElements(VT,citf.GetElementsOfType(IF))
extdof = ~cutdof
uu,vv = VT.TnT()

P = CoefficientFunction((1-nh[0]*nh[0],-nh[0]*nh[1],-nh[1]*nh[0],1-nh[1]*nh[1]), dims=(2,2))
ex = CoefficientFunction((1,0))
ey = CoefficientFunction((0,1))

#if q > k:
#    gamma = factorial(k)**2 * h 
#else:
#    gamma = factorial(k)**2

gamma = 1.0

if gp_type:
    gp = (dn(uu,1) + dn(uu.Other(),1)) * (dn(vv,1) + dn(vv.Other(),1))
    for j in range(2,min(k,q)+1): 
        gp += h**(2*j) * (dn(uu,j) - dn(uu.Other(),j)) * (dn(vv,j) - dn(vv.Other(),j))
else:
    gp = gamma * h**(-2) * (uu - uu.Other()) * (vv - vv.Other())


a = BilinearForm(VT,check_unused=False)
fx = LinearForm(VT)
fy = LinearForm(VT)

a += SymbolicBFI(levelset_domain = itf, form = uu * vv, definedonelements = citf.GetElementsOfType(IF))
a += SymbolicBFI(form = gamma * h * (grad(uu)*nh) * (grad(vv)*nh), definedonelements = citf.GetElementsOfType(IF)) # normal diffusion
if gp_type:
    a += SymbolicBFI(form = gp, VOL_or_BND = VOL, skeleton = True, definedonelements = GetFacetsWithNeighborTypes(mesh, a = citf.GetElementsOfType(IF), b = citf.GetElementsOfType(IF))) # ghost penalty
else:
    a += SymbolicFacetPatchBFI(form = gp, skeleton = False, definedonelements = GetFacetsWithNeighborTypes(mesh, a = citf.GetElementsOfType(IF), b = citf.GetElementsOfType(IF))) # ghost penalty

fx += SymbolicLFI(levelset_domain = itf, form = InnerProduct(P*ex,P*grad(vv)), definedonelements = citf.GetElementsOfType(IF))
fy += SymbolicLFI(levelset_domain = itf, form = InnerProduct(P*ey,P*grad(vv)), definedonelements = citf.GetElementsOfType(IF))

adapt = LevelSetMeshAdaptation(mesh,order=q,threshold=1e1)
deform = adapt.CalcDeformation(phi(x,y,t))
undeform = adapt.CalcDeformation(0)

mesh.SetDeformation(deform)

a.Assemble()
fx.Assemble()
fy.Assemble()

mesh.UnsetDeformation()
    
Hx = GridFunction(VT)
Hy = GridFunction(VT)

inv = a.mat.Inverse(cutdof)

Hx.vec[:] = 0.0
Hx.vec.data = inv * fx.vec

Hy.vec[:] = 0.0
Hy.vec.data = inv * fy.vec

H = CoefficientFunction((Hx,Hy)) # mean curvature vector
Draw(H,mesh,"curvature")
visoptions.deformation = 0
visoptions.autoscale = 0
visoptions.mminval = 0
visoptions.mmaxval = 0



# Extension

b = BilinearForm(VT,symmetric=True,check_unused=False)
b += SymbolicBFI(form = (grad(uu)*nh) * (grad(vv)*nh), definedonelements = citf.GetElementsOfType(POS) | citf.GetElementsOfType(NEG))

#c = SymbolicFacetPatchBFI(form = (uu-uu.Other())*(vv-vv.Other()), skeleton=False, definedonelements = GetFacetsWithNeighborTypes(mesh,a=citf.GetElementsOfType(ANY),b=citf.GetElementsOfType(ANY)))
 

mesh.SetDeformation(deform)

b.Assemble()
#c.Assemble()

mesh.UnsetDeformation()

rhs = Hx.vec.CreateVector()
rhs.data = - b.mat * Hx.vec
EHx = Hx.vec.CreateVector()
EHx.data = b.mat.Inverse(extdof,inverse="umfpack") * rhs
Hx.vec.data += EHx

rhs = Hy.vec.CreateVector()
rhs.data = - b.mat * Hy.vec
EHy = Hy.vec.CreateVector()
EHy.data = b.mat.Inverse(extdof,inverse="umfpack") * rhs
Hy.vec.data += EHy

Gx = GridFunction(VT)
Gy = GridFunction(VT)
G = CoefficientFunction((Gx,Gy))
Gx.vec.data = Hx.vec
Gy.vec.data = Hy.vec
Hx.Set(shifted_eval(Gx,deform,undeform))
Hy.Set(shifted_eval(Gy,deform,undeform))       

w = -H # extended and transformed velocity field
Draw(w,mesh,"velocity")
visoptions.deformation = 0
visoptions.autoscale = 0
visoptions.mminval = 0
visoptions.mmaxval = 0



# RKDG

VDG = L2(mesh,order=p,dirichlet=[],dgjumps=True) # DG space
u,v = VDG.TnT()
A = BilinearForm(VDG,nonassemble=True)
A += - w*u*grad(v) * dx + (w*n)*v*IfPos(w*n,u,u.Other(bnd=phi(x,y,t))) * dx(element_boundary=True)

gfu = GridFunction(VDG)
gfu.Set(phi(x,y,t))
Draw(gfu,mesh,"levelset")
visoptions.deformation = 0
visoptions.autoscale = 0
visoptions.mminval = 0
visoptions.mmaxval = 0

VCG = H1(mesh,order=p,dirichlet=[],dgjumps=False) # CG space
gfv = GridFunction(VCG)
gfv.Set(gfu)

invm = VDG.Mass(1).Inverse()
r0 = gfu.vec.CreateVector()
r1 = gfu.vec.CreateVector()
r2 = gfu.vec.CreateVector()
r3 = gfu.vec.CreateVector()

time = 0
dt = 1e-3
T = 1.0

while time < T-0.5*dt and sqrt(c*c - 2*time) > c/8:
    
    t.Set(time)
    r0 = - invm @ A.mat * gfu.vec
    
    t.Set(time + 0.5*dt)
    r1 = - invm @ A.mat * (gfu.vec + 0.5*dt*r0)
    
    t.Set(time + 0.5*dt)
    r2 = - invm @ A.mat * (gfu.vec + 0.5*dt*r1)
    
    t.Set(time + dt)
    r3 = - invm @ A.mat * (gfu.vec + dt*r2)
    
    gfu.vec.data += 1/6 * dt * (r0 + 2*r1 + 2*r2 + r3)
    #gfu.vec.data += dt * r0

    gfv.Set(gfu)
    InterpolateToP1(gfv,lset)
    citf.Update(lset)
    
    cutdof = freedof & GetDofsOfElements(VT,citf.GetElementsOfType(IF))
    extdof = ~cutdof

    a = BilinearForm(VT,check_unused=False)
    fx = LinearForm(VT)
    fy = LinearForm(VT)

    a += SymbolicBFI(levelset_domain = itf, form = uu * vv, definedonelements = citf.GetElementsOfType(IF))
    a += SymbolicBFI(form = gamma * h * (grad(uu)*nh) * (grad(vv)*nh), definedonelements = citf.GetElementsOfType(IF)) # normal diffusion
    if gp_type:
        a += SymbolicBFI(form = gp, VOL_or_BND = VOL, skeleton = True, definedonelements = GetFacetsWithNeighborTypes(mesh, a = citf.GetElementsOfType(IF), b = citf.GetElementsOfType(IF))) # ghost penalty
    else:
        a += SymbolicFacetPatchBFI(form = gp, skeleton = False, definedonelements = GetFacetsWithNeighborTypes(mesh, a = citf.GetElementsOfType(IF), b = citf.GetElementsOfType(IF))) # ghost penalty

    fx += SymbolicLFI(levelset_domain = itf, form = InnerProduct(P*ex,P*grad(vv)), definedonelements = citf.GetElementsOfType(IF))
    fy += SymbolicLFI(levelset_domain = itf, form = InnerProduct(P*ey,P*grad(vv)), definedonelements = citf.GetElementsOfType(IF))

    deform = adapt.CalcDeformation(gfv)
    undeform = adapt.CalcDeformation(0)

    mesh.SetDeformation(deform)

    a.Assemble()
    fx.Assemble()
    fy.Assemble()

    mesh.UnsetDeformation()
    
    Hx = GridFunction(VT)
    Hy = GridFunction(VT)

    inv = a.mat.Inverse(cutdof)

    Hx.vec[:] = 0.0
    Hx.vec.data = inv * fx.vec

    Hy.vec[:] = 0.0
    Hy.vec.data = inv * fy.vec

    H = CoefficientFunction((Hx,Hy))

    b = BilinearForm(VT,symmetric=True,check_unused=False)
    b += SymbolicBFI(form = (grad(uu)*nh) * (grad(vv)*nh), definedonelements = citf.GetElementsOfType(POS) | citf.GetElementsOfType(NEG))
    
    mesh.SetDeformation(deform)
    
    b.Assemble()
    
    mesh.UnsetDeformation()

    rhs = Hx.vec.CreateVector()
    rhs.data = - b.mat * Hx.vec
    EHx = Hx.vec.CreateVector()
    EHx.data = b.mat.Inverse(extdof,inverse="umfpack") * rhs
    Hx.vec.data += EHx

    rhs = Hy.vec.CreateVector()
    rhs.data = - b.mat * Hy.vec
    EHy = Hy.vec.CreateVector()
    EHy.data = b.mat.Inverse(extdof,inverse="umfpack") * rhs
    Hy.vec.data += EHy
    
    Gx.vec.data = Hx.vec
    Gy.vec.data = Hy.vec
    Hx.Set(shifted_eval(Gx,deform,undeform))
    Hy.Set(shifted_eval(Gy,deform,undeform))      

    w = -H
    
    #input("")
    print("time=",time)
    time += dt
    
    Redraw(blocking=True)

