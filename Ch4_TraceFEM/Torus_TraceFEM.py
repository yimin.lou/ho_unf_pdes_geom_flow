"""
This is the python script for the convergence study of the discrete mean curvature vector on a torus. The discrete mean curvature vector is computed by using the isoparametric TraceFEM developed in Chapter 4 of [1] and compared to the exact mean curvature vector.

PDE problem:
------------
The discrete mean curvature vector H is defined by

    H = - Laplacian_Gamma x    for x in Gamma := {x | phi(x) = 0}
    
where Laplacian_Gamma stands for the Laplace-Beltrami operator on the surface domain Gamma.

Domain:
-------
The surface domain Gamma is a level-set torus centered on the origin of a background cube domain [-2.0,2.0]^3 with unstructured computational mesh consisting of tetrahedrons.

L2 Error:
---------
The L2 Error is measured by

    Error = integral (E - H)^2 over Gamma_h

where the exact mean curvature vector E = Laplacian(phi) * grad(phi) and Gamma_h is the isoparametrically mapped discrete surface for a high-order approximation to Gamma.

Literature:
-----------
[1] Yimin Lou. High-order Unfitted Discretizations for Partial Differential Equations Coupled with Geometric Flow. Doctoral Thesis, Goettingen, 2021.
"""

# ------------------------------ LOAD LIBRARIES -------------------------------
from netgen.geom2d import SplineGeometry
from netgen.csg import *
from ngsolve import *
from xfem import *
from xfem.lsetcurv import *
from xfem.lset_spacetime import *
from math import pi, ceil
import numpy as np
from numpy import savetxt, concatenate, array, c_
ngsglobals.msg_level = 0

from netgen import gui

# -------------------------------- PARAMETERS ---------------------------------
# Mesh diameter
hmax = 1.0
# Polynomial order of finite element discretization
k = 3
# Polynomial order of isoparametric mapping
q = k
#
direct = True
gamma = 1

# Background domain 
domain = CSGeometry()
domain.Add(OrthoBrick(Pnt(-2,-2,-2), Pnt(2,2,2)))
mesh = Mesh(domain.GenerateMesh(maxh=hmax))
Draw(mesh)

# Radius
R = 1.0
r = 0.5
# Level set function
phi = sqrt((sqrt(x*x+y*y) - R)**2 + z*z) - r
Draw(phi, mesh, "levelset", autoscale=False, min=0, max=0)
grad_phi = CoefficientFunction((phi.Diff(x),phi.Diff(y),phi.Diff(z)))
laplace_phi = grad_phi[0].Diff(x) + grad_phi[1].Diff(y) + grad_phi[2].Diff(z)
# Exact mean curvature vector
E = laplace_phi * grad_phi

# ----------------------------------- MAIN ------------------------------------
# Error and convergence rate
err = []
rate = []

# Maximum mesh refinement level
ref = 5

for l in range(ref): 

	print("Refinement Level:", l)

	if l > 0: mesh.Refine()

	# P1 interpolation of the level set function
	lset = GridFunction(H1(mesh,order=1))
	InterpolateToP1(phi, lset)
	#Draw(lset, mesh, "lset_P1", autoscale=False, min=0, max=0, deformation=True)
		
	zero = {"levelset":lset, "domain_type":IF, "subdivlvl":0}
	neg = {"levelset":lset, "domain_type":NEG, "subdivlvl":0}
	pos = {"levelset":lset, "domain_type":POS, "subdivlvl":0}

	# Cut information
	ci = CutInfo(mesh,lset)

	# Class to compute mesh deformation adapted to the level set function
	adap = LevelSetMeshAdaptation(mesh, order = q, threshold = 1e-1)
	deform = adap.CalcDeformation(phi)
	#lset = adap.lset_p1

	# Background/trace finite element space and trial/test functions
	Vh = H1(mesh, order=k, dirichlet=[], dgjumps=True)
	Vtr = Compress(Vh,GetDofsOfElements(Vh,ci.GetElementsOfType(IF)))
	dof = Vtr.FreeDofs()
	u = Vtr.TrialFunction()
	v = Vtr.TestFunction()

	# Unit normal vector and mesh size parameter
	n = Normalize(grad(lset))
	h = specialcf.mesh_size
	#P = CoefficientFunction((1-n[0]*n[0],-n[0]*n[1],-n[0]*n[2],-n[1]*n[0],1-n[1]*n[1],-n[1]*n[2],-n[2]*n[0],-n[2]*n[1],1-n[2]*n[2]), dims=(3,3))
	P = Id(3) - OuterProduct(n,n)
	ex = CoefficientFunction((1,0,0))
	ey = CoefficientFunction((0,1,0))
	ez = CoefficientFunction((0,0,1))

	# Bilinear form
	a = BilinearForm(Vtr, check_unused=False)
	a += SymbolicBFI(levelset_domain = zero, form = u * v, definedonelements = ci.GetElementsOfType(IF))
	# Normal diffusion
	a += SymbolicBFI(form = gamma * h * (grad(u)*n) * (grad(v)*n), definedonelements = ci.GetElementsOfType(IF))
	# Ghost penalty direct or jump version
	 # for direct version or =False for derivative jump ghost penalty
	# L2-scale or H1-scale
	lam = -1 # for H1-scale or =1 for L2-scale
	if direct:
		a += SymbolicFacetPatchBFI(form = gamma * h**(lam-2) * (u - u.Other()) * (v - v.Other()), skeleton = False, definedonelements = GetFacetsWithNeighborTypes(mesh, a = ci.GetElementsOfType(IF), b = ci.GetElementsOfType(IF)))
		#a += (gamma * h**(lam-2) * (u - u.Other()) * (v - v.Other())) * dw
	else:
		for j in range(1,k+1):
			a += SymbolicBFI(form = gamma * h**lam * h**(2*j-1) * (dn(u,j,comp=[]) + (2*(j%2)-1)*dn(u.Other(),j,comp=[])) * (dn(v,j,comp=[]) + (2*(j%2)-1)*dn(v.Other(),j,comp=[])), VOL_or_BND = VOL, skeleton = True, definedonelements = GetFacetsWithNeighborTypes(mesh, a = ci.GetElementsOfType(IF), b = ci.GetElementsOfType(IF)))

	# Linear forms
	fx = LinearForm(Vtr)
	fx += SymbolicLFI(levelset_domain = zero, form = InnerProduct(P*ex,P*grad(v)), definedonelements = ci.GetElementsOfType(IF))
	fy = LinearForm(Vtr)
	fy += SymbolicLFI(levelset_domain = zero, form = InnerProduct(P*ey,P*grad(v)), definedonelements = ci.GetElementsOfType(IF))
	fz = LinearForm(Vtr)
	fz += SymbolicLFI(levelset_domain = zero, form = InnerProduct(P*ez,P*grad(v)), definedonelements = ci.GetElementsOfType(IF))

	mesh.SetDeformation(deform)
	a.Assemble()
	fx.Assemble()
	fy.Assemble()
	fz.Assemble()
	mesh.UnsetDeformation()

	# Discrete/exact mean curvature vector computation
	inv = a.mat.Inverse(dof)

	Hx = GridFunction(Vtr)
	Hx.vec[:] = 0.0
	Hx.vec.data = inv * fx.vec
	Hy = GridFunction(Vtr)
	Hy.vec[:] = 0.0
	Hy.vec.data = inv * fy.vec
	Hz = GridFunction(Vtr)
	Hz.vec[:] = 0.0
	Hz.vec.data = inv * fz.vec

	H = CoefficientFunction((Hx,Hy,Hz))
	Draw(H, mesh, "num", autoscale=True, min=-1, max=1, deformation=deform)

	# L2 error computation
	mesh.SetDeformation(deform)
	error = sqrt( Integrate((H-E)*(H-E) * dCut(levelset=lset, domain_type=IF, order=8), mesh=mesh) )
	mesh.UnsetDeformation()
	print("L2 Error: {0}".format(error))

	# Experimental order of convergence
	if l > 0: 
		eoc = log(error/errold) / log(1/2)
		print("Experimental Order of Convergence: {0}".format(eoc))
		rate.append(eoc)
		
	errold = error
	err.append(error)
	RefineAtLevelSet(adap.lset_p1)

	# Post processing
	vtk = VTKOutput(mesh, coefs=[lset,deform,E,H], names=["lset","deformation","E","H"], filename="torus", subdivision=2)#, floatsize="single", legacy=False)
	vtk.Do(drawelems=ci.GetElementsOfType(IF))
	RefineAtLevelSet(adap.lset_p1)
	Redraw(blocking=True)
			
# Data export
output_err = c_[err]
output_eoc = c_[rate]
#savetxt("T_err"+"_k"+str(k)+"_q"+str(q)+".dat", output_err)
savetxt("T_err"+"_k"+str(k)+"_q"+str(q)+"_T.dat", output_err.transpose())
savetxt("T_eoc"+"_k"+str(k)+"_q"+str(q)+".dat", output_eoc)
