"""
This is the python script for the convergence study of the discrete mean curvature vector on the unit circle. The discrete mean curvature vector is computed by using the isoparametric TraceFEM developed in Chapter 4 of [1] and compared to the exact mean curvature vector.

PDE problem:
------------
The discrete mean curvature vector H is defined by

    H = - Laplacian_Gamma x    for x in Gamma
    
where Laplacian_Gamma stands for the Laplace-Beltrami operator on the surface domain Gamma.

Domain:
-------
The surface domain Gamma is a level-set unit circle centered on the origin of a background square domain [-1.2,1.2]^2 with unstructured computational mesh consisting of triangles.

L2 Error:
---------
The L2 Error is measured by

    Error = integral (E - H)^2 over Gamma_h

where the exact mean curvature vector E = (x,y) and Gamma_h is the isoparametrically mapped discrete surface for a high-order approximation to Gamma.

Literature:
-----------
[1] Yimin Lou. High-order Unfitted Discretizations for Partial Differential Equations Coupled with Geometric Flow. Doctoral Thesis, Goettingen, 2021.
"""

# ------------------------------ LOAD LIBRARIES -------------------------------
from math import pi
from ngsolve import *
from netgen.geom2d import *
from xfem import *
from xfem.lsetcurv import *
import numpy as np
from numpy import savetxt, concatenate, array, c_

# -------------------------------- PARAMETERS ---------------------------------
# Mesh diameter
hmax = 0.6
# Polynomial order of finite element discretization
k = 3
# Polynomial order of isoparametric mapping
q = 5

# Background domain
domain = SplineGeometry()
domain.AddRectangle([-1.2,-1.2],[1.2,1.2])
mesh = Mesh(domain.GenerateMesh(maxh=hmax, quad_dominated=False))
Draw(mesh)

# Radius
r = 1.0
# Level set function
phi = x*x + y*y - r*r
Draw(phi, mesh, "levelset", autoscale=False, min=0, max=0, deformation=True)

# ----------------------------------- MAIN ------------------------------------
# Error and convergence rate
err = []
rate = []

# Maximum mesh refinement level
ref = 8

for l in range(ref):

    print("Refinement Level:", l)
    # Mesh refinement since the second level
    if l > 0: mesh.Refine()
    
    # P1 interpolation of the level set function
    lset = GridFunction(H1(mesh, order=1))
    InterpolateToP1(phi, lset)
    Draw(lset, mesh, "lset_P1", autoscale=False, min=0, max=0, deformation=True)
    
    #
    #zero = {"levelset":lset, "domain_type":IF, "subdivlvl":0}
    #neg = {"levelset":lset, "domain_type":NEG, "subdivlvl":0}
    #pos = {"levelset":lset, "domain_type":POS, "subdivlvl":0}
    
    # Cut information
    ci = CutInfo(mesh, lset)
    
    # Class to compute mesh deformation adapted to the level set function
    adap = LevelSetMeshAdaptation(mesh, order = q, threshold = 1e2)
    deform = adap.CalcDeformation(phi)
    #lset = adap.lset_p1

    # Background/trace finite element space and trial/test functions
    Vh = H1(mesh, order=k, dirichlet=[], dgjumps=True)
    Vtr = Compress(Vh, GetDofsOfElements(Vh,ci.GetElementsOfType(IF)))
    dof = Vtr.FreeDofs()
    u, v = Vtr.TnT()
    
    # Unit normal vector and mesh size parameter
    n = Normalize(grad(lset))
    h = specialcf.mesh_size
    
    # Tangential projection and Cartesian unit vectors
    P = CoefficientFunction((1-n[0]*n[0], -n[0]*n[1], -n[1]*n[0], 1-n[1]*n[1]), dims=(2,2))
    ex = CoefficientFunction((1,0))
    ey = CoefficientFunction((0,1))
    #P = lambda u: u - (u*n)*n
    
    # Measure on cut elements, interface, and facet patch
    dx = dx(definedonelements=ci.GetElementsOfType(IF), deformation=deform)
    ds = dCut(levelset=lset, domain_type=IF, definedonelements=ci.GetElementsOfType(IF), deformation=deform)
    dw = dFacetPatch(definedonelements=GetFacetsWithNeighborTypes(mesh, a=ci.GetElementsOfType(IF),b=ci.GetElementsOfType(IF)), deformation=deform)
    
    # Stabilization coefficient
    gamma = 1
    
    # Bilinear form
    a = BilinearForm(Vtr, check_unused=False)
    #a += SymbolicBFI(levelset_domain = zero, form = u * v, definedonelements = ci.GetElementsOfType(IF))
    a += u * v * ds
    # Normal diffusion stabilization
    #a += SymbolicBFI(form = gamma * h * (grad(u)*n) * (grad(v)*n), definedonelements = ci.GetElementsOfType(IF))
    a += gamma * h * (grad(u)*n) * (grad(v)*n) * dx
    
    # Ghost penalty direct or jump version
    direct = True # for direct version or =False for derivative jump ghost penalty
    # L2-scale or H1-scale
    lam = -1 # for H1-scale or =1 for L2-scale
    
    if direct:
        #a += SymbolicFacetPatchBFI(form = gamma * h**(lam-2) * (u - u.Other()) * (v - v.Other()), skeleton = False, definedonelements = GetFacetsWithNeighborTypes(mesh, a = ci.GetElementsOfType(IF), b = ci.GetElementsOfType(IF)))
        a += (gamma * h**(lam-2) * (u - u.Other()) * (v - v.Other())) * dw
        
    else:
        for j in range(1,k+1):
            a += SymbolicBFI(form = gamma * h**lam * h**(2*j-1) * (dn(u,j,comp=[]) + (2*(j%2)-1)*dn(u.Other(),j,comp=[])) * (dn(v,j,comp=[]) + (2*(j%2)-1)*dn(v.Other(),j,comp=[])), VOL_or_BND = VOL, skeleton = True, definedonelements = GetFacetsWithNeighborTypes(mesh, a = ci.GetElementsOfType(IF), b = ci.GetElementsOfType(IF)))
    
    # Linear form
    fx = LinearForm(Vtr)
    #fx += SymbolicLFI(levelset_domain = zero, form = InnerProduct(P*ex,P*grad(v)), definedonelements = ci.GetElementsOfType(IF))
    fx += InnerProduct(P*ex,P*grad(v)) * ds
    fy = LinearForm(Vtr)
    #fy += SymbolicLFI(levelset_domain = zero, form = InnerProduct(P*ey,P*grad(v)), definedonelements = ci.GetElementsOfType(IF))
    fy += InnerProduct(P*ey,P*grad(v)) * ds
    
    # Bilinear/linear form assemble
    #mesh.SetDeformation(deform)
    a.Assemble()
    fx.Assemble()
    fy.Assemble()
    #mesh.UnsetDeformation()
    
    # Discrete/exact mean curvature vector computation
    inv = a.mat.Inverse(dof)
    
    Hx = GridFunction(Vtr)
    Hx.vec[:] = 0.0
    Hx.vec.data = inv * fx.vec
    
    Hy = GridFunction(Vtr)
    Hy.vec[:] = 0.0
    Hy.vec.data = inv * fy.vec

    H = CoefficientFunction((Hx,Hy))
    Draw(H, mesh, "num", autoscale=True, min=-1, max=1, deformation=deform)
    
    E = CoefficientFunction((x,y))
    Draw(E, mesh, "exa", autoscale=True, min=-1, max=1, deformation=deform)
    
    # L2 error computation
    #mesh.SetDeformation(deform)
    error = sqrt( Integrate((H-E)*(H-E) * ds.order(8), mesh=mesh) )
    print("L2 Error: {:e}".format(error))
    #mesh.UnsetDeformation()
    
    # Experimental order of convergence
    if l > 0:
        eoc = log(error/errold) / log(1/2)
        print("Experimental Order of Convergence: {:.2f}".format(eoc))
        rate.append(eoc)
        
    errold = error
    err.append(error)
    RefineAtLevelSet(adap.lset_p1)
    
    # Post processing
    vtk = VTKOutput(mesh, coefs=[lset,deform,E,H], names=["lset","deformation","E","H"], filename="circle", subdivision=2, floatsize="single", legacy=False)
    vtk.Do(drawelems=ci.GetElementsOfType(IF))

    #test = sqrt( Integrate( CoefficientFunction((x*y,2*x*y))**2 * ds.order(k+q), mesh=mesh) )  #dCut(lset, IF, definedonelements=ci.GetElementsOfType(IF), deformation=deform), mesh=mesh) ) #
    #print("Test: ", test)
    #Redraw(blocking=True)
    
# Data output
output_err = c_[err]
output_eoc = c_[rate]
savetxt("err"+"_k"+str(k)+"_q"+str(q)+"_lam"+str(lam)+"_"+str(direct)+".dat", output_err)
savetxt("err"+"_k"+str(k)+"_q"+str(q)+"_lam"+str(lam)+"_"+str(direct)+".dat", output_err.transpose())
savetxt("eoc"+"_k"+str(k)+"_q"+str(q)+"_lam"+str(lam)+"_"+str(direct)+".dat", output_eoc)
